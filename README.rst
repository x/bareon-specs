===============================
bareon
===============================

Bareon provides flexible and data driven interface to perform actions which are related to operating system installation

* Free software: Apache license
* Documentation: https://wiki.openstack.org/wiki/Bareon
